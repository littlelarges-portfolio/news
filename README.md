# News

[![style: very good analysis](https://img.shields.io/badge/style-very_good_analysis-B22C89.svg)](https://pub.dev/packages/very_good_analysis)

## 🔍 Overview

The News App is a mobile application developed using Flutter, a popular open-source UI software development kit. It provides a simple and intuitive interface for users to browse through a news feed and view detailed news articles. The application is designed with Clean Architecture and Test-Driven Development (TDD) principles to ensure robustness and maintainability.

## 🌟 Application Highlights

### Technical Foundations

- Clean Architecture: Ensures that the app is scalable, maintainable, and decoupled, with a clear separation of concerns that facilitates independent development, testing, and maintenance.

- Test-Driven Development (TDD): Adopts a TDD approach for building reliable and bug-free code, emphasizing the importance of writing tests before the actual implementation to ensure that the app functions as expected.

- Adaptive Layout: Incorporates a responsive and flexible layout design, making the app compatible and user-friendly across a wide range of devices and screen sizes.

- Data Caching: Implements caching mechanisms to enhance performance and user experience, enabling faster load times and offline access to previously loaded news articles.

### User Experience Features

- News Feed: The app displays a continuous feed of news articles, allowing users to stay updated with the latest information.
  
- Article Details: Users can view detailed information about a specific news article by tapping on it in the feed.

## :iphone: Screens

|             Home             |             Article             |
| :--------------------------: | :-----------------------------: |
| ![](./readme_files/home.png) | ![](./readme_files/article.png) |

## 🔫 Getting Started

Follow these steps to get your development environment set up:

### Prerequisites

- Ensure you have Flutter installed on your machine. If you don't have Flutter installed, follow the instructions on the Flutter official website.
- An API key from newsapi.org. Sign up on their website to obtain your free API key.

### Installation

1. Clone the repository to your local machine:

```
git clone https://gitlab.com/littlelarges-portfolio/news.git
```
2. Navigate to the project directory:
```
cd news
```

3. Get the required dependencies:
```
flutter pub get
```

### Setting Up Your API Key
1. In the root of the project directory, locate the .env file. If it's not present, create it.

2. Open the '.env' file and insert your API key as follows:

```
API_KEY=your_newsapi_org_api_key_here
```

Replace 'your_newsapi_org_api_key_here' with the actual API key you obtained from 'newsapi.org'.

### Running the Application

1. Ensure you are in the project root directory.

2. Execute the following command to run the application:

```
flutter run
```
## 🛠️ Tech Stack

<table>
    <tr>
        <td>Bloc</td>
        <td>Dartz</td>
        <td>Dio</td>
        <td>Get It</td>
    </tr>
    <tr>
        <td>Freezed</td>
        <td>Equatable</td>
        <td>Go Router</td>
        <td>Shared Preferences</td>
    </tr>
</table>

## 📝 Conclusion

The News App is a straightforward and efficient mobile application that leverages Flutter's capabilities to provide users with a seamless news browsing experience. By adhering to Clean Architecture and Test-Driven Development, the development process has been structured to ensure high code quality and maintainability.