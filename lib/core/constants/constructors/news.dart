import 'package:news_app/core/constants/constructors/source_models.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';

class NewsEntities {
  static const empty = News(
    source: SourceModels.empty,
    author: '_empty.autor',
    title: '_empty.title',
    description: '_empty.description',
    url: '_empty.url',
    urlToImage: '_empty.urlToImage',
    publishedAt: '_empty.publishedAt',
    content: '_empty.content',
  );
}
