// ignore_for_file: avoid_redundant_argument_values

import 'package:news_app/core/constants/constructors/source_models.dart';
import 'package:news_app/core/res/links.dart';
import 'package:news_app/features/daily_news/data/models/news_model.dart';

sealed class NewsModels {
  static const empty = NewsModel(
    source: SourceModels.empty,
    author: '_empty.author',
    title: '_empty.title',
    description: '_empty.description',
    url: '_empty.url',
    urlToImage: '_empty.urlToImage',
    publishedAt: '_empty.publishedAt',
    content: '_empty.content',
  );

  static const withNull = NewsModel(
    title: 'title',
    url: 'url',
    publishedAt: 'publishedAt',
    content: 'content',
    source: SourceModels.withNull,
    author: 'Unknown author',
    description: 'No description',
    urlToImage: Links.noImage,
  );
}
