import 'package:news_app/features/daily_news/data/models/source_model.dart';

class SourceModels {
  static const empty = SourceModel(name: '_empty.name');

  static const withNull = SourceModel(name: 'Unknown');
}
