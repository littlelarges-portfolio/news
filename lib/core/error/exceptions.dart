import 'package:equatable/equatable.dart';

abstract class CustomException extends Equatable implements Exception {
  const CustomException({
    required this.message,
    required this.statusCode,
  });

  final String message;
  final String statusCode;

  @override
  List<Object?> get props => [message, statusCode];
}

class ServerException extends CustomException {
  const ServerException({required super.message, required super.statusCode});
}

class CacheException extends CustomException {
  const CacheException({required super.message, required super.statusCode});
}
