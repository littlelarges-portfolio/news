import 'package:equatable/equatable.dart';
import 'package:news_app/core/error/exceptions.dart';

abstract class Failure extends Equatable {
  const Failure({required this.message, required this.statusCode});

  final String message;
  final String statusCode;

  @override
  List<Object?> get props => [message, statusCode];
}

class ServerFailure extends Failure {
  const ServerFailure({required super.message, required super.statusCode});

  factory ServerFailure.fromException(CustomException e) =>
      ServerFailure(message: e.message, statusCode: e.statusCode);
}

class CacheFailure extends Failure {
  const CacheFailure({required super.message, required super.statusCode});

  factory CacheFailure.fromException(CustomException e) =>
      CacheFailure(message: e.message, statusCode: e.statusCode);
}
