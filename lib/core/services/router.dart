import 'package:go_router/go_router.dart';
import 'package:news_app/core/constants/page_paths.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';
import 'package:news_app/features/daily_news/presentation/pages/article_page.dart';
import 'package:news_app/features/daily_news/presentation/pages/home_page.dart';

final router = GoRouter(
  routes: [
    GoRoute(
      path: PagePaths.home,
      builder: (context, state) => const HomePage(title: 'Daily News'),
    ),
    GoRoute(
      path: PagePaths.article,
      builder: (context, state) =>
          ArticlePage(title: 'Details', news: state.extra! as News),
    ),
  ],
);
