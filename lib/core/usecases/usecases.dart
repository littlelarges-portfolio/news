abstract class UsecaseWithoutParams<ReturnType> {
  ReturnType call();
}

abstract class UsecaseWithParams<ReturnType, Params> {
  ReturnType call(Params params);
}
