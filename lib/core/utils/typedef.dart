import 'package:dartz/dartz.dart';
import 'package:news_app/core/error/failures.dart';

/// Returns Future<Either<Failure, T>>
typedef ResultFuture<T> = Future<Either<Failure, T>>;
