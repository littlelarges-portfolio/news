// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:news_app/core/constants/cache_keys.dart';
import 'package:news_app/core/error/exceptions.dart';
import 'package:news_app/features/daily_news/data/models/news_model.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class DailyNewsLocalDataSource {
  Future<List<NewsModel>> getNews();
  Future<void> cacheNews(List<News> cacheToNews);
}

class DailyNewsLocalDataSourceImpl implements DailyNewsLocalDataSource {
  final SharedPreferences _sharedPreferences;

  DailyNewsLocalDataSourceImpl({required SharedPreferences sharedPreferences})
      : _sharedPreferences = sharedPreferences;

  @override
  Future<List<NewsModel>> getNews() {
    final newsJson = _sharedPreferences.getStringList(CacheKeys.news);

    if (newsJson != null) {
      final news = newsJson.map((e) {
        final json = jsonDecode(e) as Map<String, dynamic>;

        return NewsModel.fromJson(json);
      }).toList();

      return Future.value(news);
    } else {
      throw const CacheException(message: 'Not found!', statusCode: '404');
    }
  }

  @override
  Future<void> cacheNews(List<News> cacheToNews) {
    return _sharedPreferences.setStringList(
      CacheKeys.news,
      cacheToNews.map(jsonEncode).toList(),
    );
  }
}
