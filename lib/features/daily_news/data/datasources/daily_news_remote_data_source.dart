import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:news_app/core/constants/api.dart';
import 'package:news_app/core/error/exceptions.dart';
import 'package:news_app/features/daily_news/data/models/news_model.dart';

abstract class DailyNewsRemoteDataSource {
  Future<List<NewsModel>> getNews();
}

class DailyNewsRemoteDataSourceImpl implements DailyNewsRemoteDataSource {
  DailyNewsRemoteDataSourceImpl({required Dio httpClient})
      : _httpClient = httpClient;

  final Dio _httpClient;

  @override
  Future<List<NewsModel>> getNews() async {
    try {
      final res = await _httpClient
          .get<Map<String, dynamic>>('${Api.url}&apiKey=${dotenv.get(
        'API_KEY',
      )}');

      return (res.data?['articles'] as List)
          .map((e) => e as Map<String, dynamic>)
          .map(NewsModel.fromJson)
          .toList();
    } on DioException catch (e) {
      throw ServerException(
        message: e.message ?? 'Something went wrong...',
        statusCode: (e.response?.statusCode ?? 404).toString(),
      );
    }
  }
}
