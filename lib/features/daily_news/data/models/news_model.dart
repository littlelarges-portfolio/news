import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:news_app/core/res/links.dart';
import 'package:news_app/features/daily_news/data/models/source_model.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';

part 'news_model.freezed.dart';
part 'news_model.g.dart';

@Freezed(copyWith: false)
class NewsModel extends News with _$NewsModel {
  @JsonSerializable(explicitToJson: true)
  const factory NewsModel({
    required String title,
    required String url,
    required String publishedAt,
    required String content,
    @Default(SourceModel(name: 'Unknown')) SourceModel source,
    @Default('Unknown author') String author,
    @Default('No description') String description,
    @Default(Links.noImage) String urlToImage,
  }) = _NewsModel;

  factory NewsModel.fromJson(Map<String, dynamic> json) =>
      _$NewsModelFromJson(json);
}
