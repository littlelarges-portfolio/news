// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'news_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

NewsModel _$NewsModelFromJson(Map<String, dynamic> json) {
  return _NewsModel.fromJson(json);
}

/// @nodoc
mixin _$NewsModel {
  String get title => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;
  String get publishedAt => throw _privateConstructorUsedError;
  String get content => throw _privateConstructorUsedError;
  SourceModel get source => throw _privateConstructorUsedError;
  String get author => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  String get urlToImage => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$NewsModelImpl implements _NewsModel {
  const _$NewsModelImpl(
      {required this.title,
      required this.url,
      required this.publishedAt,
      required this.content,
      this.source = const SourceModel(name: 'Unknown'),
      this.author = 'Unknown author',
      this.description = 'No description',
      this.urlToImage = Links.noImage});

  factory _$NewsModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$NewsModelImplFromJson(json);

  @override
  final String title;
  @override
  final String url;
  @override
  final String publishedAt;
  @override
  final String content;
  @override
  @JsonKey()
  final SourceModel source;
  @override
  @JsonKey()
  final String author;
  @override
  @JsonKey()
  final String description;
  @override
  @JsonKey()
  final String urlToImage;

  @override
  String toString() {
    return 'NewsModel(title: $title, url: $url, publishedAt: $publishedAt, content: $content, source: $source, author: $author, description: $description, urlToImage: $urlToImage)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NewsModelImpl &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.url, url) || other.url == url) &&
            (identical(other.publishedAt, publishedAt) ||
                other.publishedAt == publishedAt) &&
            (identical(other.content, content) || other.content == content) &&
            (identical(other.source, source) || other.source == source) &&
            (identical(other.author, author) || other.author == author) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.urlToImage, urlToImage) ||
                other.urlToImage == urlToImage));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, title, url, publishedAt, content,
      source, author, description, urlToImage);

  @override
  Map<String, dynamic> toJson() {
    return _$$NewsModelImplToJson(
      this,
    );
  }
}

abstract class _NewsModel implements NewsModel {
  const factory _NewsModel(
      {required final String title,
      required final String url,
      required final String publishedAt,
      required final String content,
      final SourceModel source,
      final String author,
      final String description,
      final String urlToImage}) = _$NewsModelImpl;

  factory _NewsModel.fromJson(Map<String, dynamic> json) =
      _$NewsModelImpl.fromJson;

  @override
  String get title;
  @override
  String get url;
  @override
  String get publishedAt;
  @override
  String get content;
  @override
  SourceModel get source;
  @override
  String get author;
  @override
  String get description;
  @override
  String get urlToImage;
}
