// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$NewsModelImpl _$$NewsModelImplFromJson(Map<String, dynamic> json) =>
    _$NewsModelImpl(
      title: json['title'] as String,
      url: json['url'] as String,
      publishedAt: json['publishedAt'] as String,
      content: json['content'] as String,
      source: json['source'] == null
          ? const SourceModel(name: 'Unknown')
          : SourceModel.fromJson(json['source'] as Map<String, dynamic>),
      author: json['author'] as String? ?? 'Unknown author',
      description: json['description'] as String? ?? 'No description',
      urlToImage: json['urlToImage'] as String? ?? Links.noImage,
    );

Map<String, dynamic> _$$NewsModelImplToJson(_$NewsModelImpl instance) =>
    <String, dynamic>{
      'title': instance.title,
      'url': instance.url,
      'publishedAt': instance.publishedAt,
      'content': instance.content,
      'source': instance.source.toJson(),
      'author': instance.author,
      'description': instance.description,
      'urlToImage': instance.urlToImage,
    };
