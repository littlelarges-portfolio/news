import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:news_app/features/daily_news/domain/entities/source.dart';

part 'source_model.freezed.dart';
part 'source_model.g.dart';

@Freezed(copyWith: false)
class SourceModel extends Source with _$SourceModel {
  @JsonSerializable(explicitToJson: true)
  const factory SourceModel({required String name}) = _Source;

  factory SourceModel.fromJson(Map<String, dynamic> json) =>
      _$SourceModelFromJson(json);
}
