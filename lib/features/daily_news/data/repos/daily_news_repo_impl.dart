import 'package:dartz/dartz.dart';
import 'package:news_app/core/error/exceptions.dart';
import 'package:news_app/core/error/failures.dart';
import 'package:news_app/core/network/network_info.dart';
import 'package:news_app/core/utils/typedef.dart';
import 'package:news_app/features/daily_news/data/datasources/daily_news_local_data_source.dart';
import 'package:news_app/features/daily_news/data/datasources/daily_news_remote_data_source.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';
import 'package:news_app/features/daily_news/domain/repos/daily_news_repo.dart';

class DailyNewsRepoImpl implements DailyNewsRepo {
  DailyNewsRepoImpl({
    required DailyNewsRemoteDataSource remoteDataSource,
    required DailyNewsLocalDataSource localDataSource,
    required NetworkInfo networkInfo,
  })  : _networkInfo = networkInfo,
        _localDataSource = localDataSource,
        _remoteDataSource = remoteDataSource;

  final DailyNewsRemoteDataSource _remoteDataSource;
  final DailyNewsLocalDataSource _localDataSource;
  final NetworkInfo _networkInfo;

  @override
  ResultFuture<List<News>> getNews() async {
    late List<News> news;

    if (await _networkInfo.isConnected) {
      try {
        news = await _remoteDataSource.getNews();
        await _localDataSource.cacheNews(news);

        return Right(news);
      } on ServerException catch (e) {
        return Left(ServerFailure.fromException(e));
      }
    } else {
      try {
        news = await _localDataSource.getNews();

        return Right(news);
      } on CacheException catch (e) {
        return Left(CacheFailure.fromException(e));
      }
    }
  }
}
