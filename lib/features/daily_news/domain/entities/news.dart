// ignore_for_file: unused_element

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:news_app/features/daily_news/domain/entities/source.dart';

part 'news.freezed.dart';

@Freezed(copyWith: false)
class News with _$News {
  const factory News({
    required Source source,
    required String author,
    required String title,
    required String description,
    required String url,
    required String urlToImage,
    required String publishedAt,
    required String content,
  }) = _News;

  const News._();
}
