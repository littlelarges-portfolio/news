// ignore_for_file: unused_element

import 'package:freezed_annotation/freezed_annotation.dart';

part 'source.freezed.dart';

@Freezed(copyWith: false)
class Source with _$Source {
  const factory Source({
    required String name,
  }) = _Source;

  const Source._();
}
