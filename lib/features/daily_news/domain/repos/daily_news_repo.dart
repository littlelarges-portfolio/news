import 'package:news_app/core/utils/typedef.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';

abstract class DailyNewsRepo {
  ResultFuture<List<News>> getNews();
}
