import 'package:news_app/core/usecases/usecases.dart';
import 'package:news_app/core/utils/typedef.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';
import 'package:news_app/features/daily_news/domain/repos/daily_news_repo.dart';

class GetNews extends UsecaseWithoutParams<ResultFuture<List<News>>> {
  GetNews(
    this._repo,
  );
  
  final DailyNewsRepo _repo;
  
  @override
  ResultFuture<List<News>> call() async => _repo.getNews();
}
