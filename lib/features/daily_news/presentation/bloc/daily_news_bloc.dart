import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:news_app/features/daily_news/domain/entities/news.dart';
import 'package:news_app/features/daily_news/domain/usecases/get_news.dart';

part 'daily_news_event.dart';
part 'daily_news_state.dart';

part 'daily_news_bloc.freezed.dart';

class DailyNewsBloc extends Bloc<DailyNewsEvent, DailyNewsState> {
  DailyNewsBloc({
    required GetNews getNews,
  })  : _getNews = getNews,
        super(const Empty()) {
    on<FetchDailyNews>(_onFetchDailyNews);
  }

  final GetNews _getNews;

  Future<void> _onFetchDailyNews(
    DailyNewsEvent event,
    Emitter<DailyNewsState> emit,
  ) async {
    emit(const Loading());

    final failureOrNews = await _getNews();

    failureOrNews.fold(
      (l) => emit(Error(message: l.message)),
      (r) => emit(Loaded(news: r)),
    );
  }
}
