part of 'daily_news_bloc.dart';

@freezed
sealed class DailyNewsEvent with _$DailyNewsEvent {
  const factory DailyNewsEvent.fetch() = FetchDailyNews;
}
