part of 'daily_news_bloc.dart';

@freezed
sealed class DailyNewsState with _$DailyNewsState {
  const factory DailyNewsState.empty() = Empty;
  const factory DailyNewsState.loading() = Loading;
  const factory DailyNewsState.loaded({required List<News> news}) = Loaded;
  const factory DailyNewsState.error({required String message}) = Error;
}
