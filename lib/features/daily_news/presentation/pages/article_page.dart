import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';
import 'package:news_app/features/daily_news/presentation/widgets/article_page/article_info_text.dart';
import 'package:news_app/features/daily_news/presentation/widgets/article_page/cached_network_image/on_error.dart';
import 'package:news_app/features/daily_news/presentation/widgets/article_page/cached_network_image/on_loading.dart';
import 'package:url_launcher/url_launcher_string.dart';

class ArticlePage extends StatelessWidget {
  const ArticlePage({required this.title, required this.news, super.key});

  final String title;
  final News news;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title, style: TextStyle(fontSize: 26.r)),
      ),
      body: Padding(
        padding: EdgeInsets.only(right: 20.r, top: 20.r, left: 20.r),
        child: SingleChildScrollView(
          child: Column(
            children: [
              IntrinsicHeight(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 78.75.r,
                      width: 140.r,
                      child: CachedNetworkImage(
                        imageUrl: news.urlToImage,
                        progressIndicatorBuilder: (context, url, progress) =>
                            const OnLoading(indicatorSize: 40, strokeWidth: 2),
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.only(
                              topRight: Radius.circular(15),
                              bottomLeft: Radius.circular(15),
                            ),
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) =>
                            const OnError(errorFontSize: 16),
                      ),
                    ),
                    SizedBox(width: 20.r),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ArticleInfoText(
                            text: 'Author: ${news.author}',
                          ),
                          ArticleInfoText(
                            text: 'Published at: ${news.publishedAt}',
                          ),
                          ArticleInfoText(
                            text: 'Source: ${news.source.name}',
                          ),
                          MouseRegion(
                            cursor: SystemMouseCursors.click,
                            child: GestureDetector(
                              onTap: () => launchUrlString(news.url),
                              child: RichText(
                                text: TextSpan(
                                  children: [
                                    WidgetSpan(
                                      child: SvgPicture.asset(
                                        'assets/svg/external_link.svg',
                                        height: 10.r,
                                        colorFilter: const ColorFilter.mode(
                                          Colors.blue,
                                          BlendMode.srcIn,
                                        ),
                                      ),
                                    ),
                                    WidgetSpan(
                                      child: SizedBox(
                                        width: 10.r,
                                      ),
                                    ),
                                    const TextSpan(
                                      text: 'Go to source',
                                    ),
                                  ],
                                  style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 8.r,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: [
                    SizedBox(height: 20.r),
                    Text(
                      news.title,
                      style: TextStyle(fontSize: 22.r),
                      maxLines: 10,
                    ),
                    SizedBox(height: 20.r),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20.r),
                      child: Text(
                        news.content,
                        style: TextStyle(
                          fontSize: 15.r,
                          height: 1.5,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
