import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:news_app/features/daily_news/presentation/bloc/daily_news_bloc.dart';
import 'package:news_app/features/daily_news/presentation/widgets/home/article_card.dart';
import 'package:news_app/injection_container.dart';

class HomePage extends StatefulWidget {
  const HomePage({required this.title, super.key});

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => sl<DailyNewsBloc>()..add(const FetchDailyNews()),
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          toolbarHeight: 60.h,
          title: Text(
            widget.title,
            style: TextStyle(
              fontSize: 26.r,
              fontWeight: FontWeight.w200,
            ),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
          ),
          child: BlocBuilder<DailyNewsBloc, DailyNewsState>(
            builder: (context, state) {
              return state.when(
                empty: Container.new,
                loading: () => const Center(child: CircularProgressIndicator()),
                loaded: (news) => ListView.separated(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  itemBuilder: (context, index) =>
                      ArticleCard(news: news[index]),
                  separatorBuilder: (context, index) => SizedBox(height: 20.h),
                  itemCount: news.length,
                ),
                error: (message) => Center(
                  child: Text(message),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
