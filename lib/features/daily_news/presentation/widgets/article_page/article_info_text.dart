import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ArticleInfoText extends StatelessWidget {
  const ArticleInfoText({required this.text, super.key});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 8.r,
        fontWeight: FontWeight.w500,
      ),
      maxLines: 4,
    );
  }
}
