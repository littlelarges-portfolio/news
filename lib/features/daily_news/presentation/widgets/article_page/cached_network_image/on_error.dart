import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OnError extends StatelessWidget {
  const OnError({required this.errorFontSize, super.key});

  final int errorFontSize;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Something went wrong...',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: errorFontSize.r),
      ),
    );
  }
}
