import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OnLoading extends StatelessWidget {
  const OnLoading({
    required this.indicatorSize,
    required this.strokeWidth,
    super.key,
  });

  final int indicatorSize;
  final int strokeWidth;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: indicatorSize.r,
        width: indicatorSize.r,
        child: CircularProgressIndicator(
          strokeWidth: strokeWidth.r,
        ),
      ),
    );
  }
}
