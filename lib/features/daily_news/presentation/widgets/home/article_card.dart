import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:news_app/core/constants/page_paths.dart';
import 'package:news_app/core/res/colours.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';
import 'package:news_app/features/daily_news/presentation/widgets/article_page/cached_network_image/on_error.dart';
import 'package:news_app/features/daily_news/presentation/widgets/article_page/cached_network_image/on_loading.dart';

class ArticleCard extends StatelessWidget {
  const ArticleCard({
    required this.news,
    super.key,
  });

  final News news;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => context.push(PagePaths.article, extra: news),
      child: UnconstrainedBox(
        child: Container(
          decoration: BoxDecoration(
            color: Colours.articleCard,
            borderRadius: const BorderRadius.only(
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(15),
            ),
          ),
          height: 104.r,
          width: 300.r,
          child: Padding(
            padding: EdgeInsets.all(10.r),
            child: Row(
              children: [
                SizedBox(
                  height: 47.25.r,
                  width: 84.r,
                  child: CachedNetworkImage(
                    imageUrl: news.urlToImage,
                    progressIndicatorBuilder: (context, url, progress) =>
                        const OnLoading(indicatorSize: 20, strokeWidth: 2),
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                        borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(15),
                          bottomLeft: Radius.circular(15),
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) =>
                        const OnError(errorFontSize: 8),
                  ),
                ),
                SizedBox(width: 10.r),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        news.title,
                        style: TextStyle(
                          fontSize: 14.r,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(height: 10.r),
                      Text(
                        news.description,
                        style: TextStyle(
                          fontSize: 8.r,
                          height: 1.5,
                        ),
                        maxLines: 4,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
