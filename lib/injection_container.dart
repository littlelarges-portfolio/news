import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:news_app/core/network/network_info.dart';
import 'package:news_app/features/daily_news/data/datasources/daily_news_local_data_source.dart';
import 'package:news_app/features/daily_news/data/datasources/daily_news_remote_data_source.dart';
import 'package:news_app/features/daily_news/data/repos/daily_news_repo_impl.dart';
import 'package:news_app/features/daily_news/domain/repos/daily_news_repo.dart';
import 'package:news_app/features/daily_news/domain/usecases/get_news.dart';
import 'package:news_app/features/daily_news/presentation/bloc/daily_news_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //! Features

  // Daily News

  sl
    ..registerFactory(
      () => DailyNewsBloc(getNews: sl()),
    )
    ..registerLazySingleton(
      () => GetNews(sl()),
    )
    ..registerLazySingleton<DailyNewsRepo>(
      () => DailyNewsRepoImpl(
        remoteDataSource: sl(),
        localDataSource: sl(),
        networkInfo: sl(),
      ),
    )
    ..registerLazySingleton<DailyNewsRemoteDataSource>(
      () => DailyNewsRemoteDataSourceImpl(httpClient: sl()),
    )
    ..registerLazySingleton<DailyNewsLocalDataSource>(
      () => DailyNewsLocalDataSourceImpl(
        sharedPreferences: sl(),
      ),
    )

    //! Core

    ..registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(
        internetConnectionChecker: sl(),
      ),
    )

    //! External

    ..registerLazySingleton(Dio.new)
    ..registerSingletonAsync(SharedPreferences.getInstance)
    ..registerLazySingleton(InternetConnectionChecker.new);

  await sl.allReady();
}
