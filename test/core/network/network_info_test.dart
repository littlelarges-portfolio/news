import 'package:flutter_test/flutter_test.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mocktail/mocktail.dart';
import 'package:news_app/core/network/network_info.dart';

class MockInternetConnectionChecker extends Mock
    implements InternetConnectionChecker {}

void main() {
  late MockInternetConnectionChecker internetConnectionChecker;
  late NetworkInfoImpl networkInfo;

  setUp(() {
    internetConnectionChecker = MockInternetConnectionChecker();
    networkInfo =
        NetworkInfoImpl(internetConnectionChecker: internetConnectionChecker);
  });

  test('should forward the call [InternetConnectionChecker.hasConnection]',
      () async {
    // arrange

    final tExpectedFuture = Future.value(true);

    when(() => internetConnectionChecker.hasConnection)
        .thenAnswer((_) => tExpectedFuture);

    // act

    final result = networkInfo.isConnected;

    //assert

    verify(() => internetConnectionChecker.hasConnection);
    expect(result, equals(tExpectedFuture));
  });
}
