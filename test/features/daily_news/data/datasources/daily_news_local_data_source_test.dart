import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:news_app/core/constants/cache_keys.dart';
import 'package:news_app/core/constants/constructors/news_models.dart';
import 'package:news_app/core/error/exceptions.dart';
import 'package:news_app/features/daily_news/data/datasources/daily_news_local_data_source.dart';
import 'package:news_app/features/daily_news/data/models/news_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

void main() {
  late MockSharedPreferences sharedPreferences;
  late DailyNewsLocalDataSourceImpl dailyNewsLocalDataSourceImpl;

  setUp(() {
    sharedPreferences = MockSharedPreferences();
    dailyNewsLocalDataSourceImpl =
        DailyNewsLocalDataSourceImpl(sharedPreferences: sharedPreferences);
  });

  group('getNews', () {
    final tNewsJson = fixture('news.json');

    final tNewsJsons = [
      tNewsJson,
    ];

    final tNewsModels = [
      NewsModel.fromJson(jsonDecode(tNewsJson) as Map<String, dynamic>),
    ];

    test(
        'should return proper [List<NewsModel>] '
        'from [SharedPreferences] when there is one', () async {
      // arrange

      when(() => sharedPreferences.getStringList(any()))
          .thenAnswer((_) => tNewsJsons);

      // act

      final result = await dailyNewsLocalDataSourceImpl.getNews();

      // assert

      expect(result, equals(tNewsModels));
      verify(() => sharedPreferences.getStringList(any())).called(1);
      verifyNoMoreInteractions(sharedPreferences);
    });

    test('should throw a [CacheException] when there is not cached value',
        () async {
      // arrange

      when(() => sharedPreferences.getStringList(any())).thenReturn(null);

      // act

      final call = dailyNewsLocalDataSourceImpl.getNews;

      // assert

      expect(call, throwsA(const TypeMatcher<CacheException>()));
      verify(() => sharedPreferences.getStringList(any())).called(1);
      verifyNoMoreInteractions(sharedPreferences);
    });
  });

  group('cacheNews', () {
    const tNewsModel = NewsModels.empty;
    final tNewsModels = [tNewsModel];
    final tExpectedJsonList = tNewsModels.map(jsonEncode).toList();

    test('should call [SharedPreferences] to cache the data', () {
      // arrange

      when(
        () =>
            sharedPreferences.setStringList(CacheKeys.news, tExpectedJsonList),
      ).thenAnswer((_) async => true);

      // act

      dailyNewsLocalDataSourceImpl.cacheNews(tNewsModels);

      // assert

      verify(
        () =>
            sharedPreferences.setStringList(CacheKeys.news, tExpectedJsonList),
      );
    });
  });
}
