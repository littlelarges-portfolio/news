import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:news_app/core/constants/constructors/news_models.dart';
import 'package:news_app/core/error/exceptions.dart';
import 'package:news_app/features/daily_news/data/datasources/daily_news_remote_data_source.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockDio extends Mock implements Dio {}

void main() {
  late MockDio mockDio;
  late DailyNewsRemoteDataSourceImpl dailyNewsRemoteDataSourceImpl;

  setUpAll(() async {
    /// Initialize test widgets to allow access to dot env.
    TestWidgetsFlutterBinding.ensureInitialized();
    
    await dotenv.load();
  });

  setUp(() {
    mockDio = MockDio();
    dailyNewsRemoteDataSourceImpl =
        DailyNewsRemoteDataSourceImpl(httpClient: mockDio);
  });

  group('getNews', () {
    final tJson = fixture('news_remote.json');
    final tJsonMap = jsonDecode(tJson) as Map<String, dynamic>;

    const tNewsModel = NewsModels.empty;
    final tNewsModels = [tNewsModel];

    test('should return proper [List<NewsModel>] when completes successfull',
        () async {
      // arrange

      when(() => mockDio.get<Map<String, dynamic>>(any())).thenAnswer(
        (_) async => Response(requestOptions: RequestOptions(), data: tJsonMap),
      );

      // act

      final result = await dailyNewsRemoteDataSourceImpl.getNews();

      // assert

      expect(result, equals(tNewsModels));
      verify(() => mockDio.get<Map<String, dynamic>>(any()));
      verifyNoMoreInteractions(mockDio);
    });

    test('should return a [ServerException] when completes unsuccessfull',
        () async {
      // arrange

      when(() => mockDio.get<Map<String, dynamic>>(any())).thenThrow(
        DioException(requestOptions: RequestOptions()),
      );

      // act

      final call = dailyNewsRemoteDataSourceImpl.getNews();

      // assert

      expect(call, throwsA(const TypeMatcher<ServerException>()));
      verify(() => mockDio.get<Map<String, dynamic>>(any()));
      verifyNoMoreInteractions(mockDio);
    });
  });
}
