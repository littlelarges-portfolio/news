import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:news_app/core/constants/constructors/news_models.dart';
import 'package:news_app/features/daily_news/data/models/news_model.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  const tNewsModel = NewsModels.empty;
  const tNewsModelWithNull = NewsModels.withNull;

  test('should be a subclass of [News] entity', () async {
    // assert

    expect(tNewsModel, isA<News>());
  });

  group('from json', () {
    test('should return a valid [NewsModel]', () {
      // arrange

      final jsonMap = jsonDecode(fixture('news.json')) as Map<String, dynamic>;

      // act

      final result = NewsModel.fromJson(jsonMap);

      // assert

      expect(result, equals(tNewsModel));
    });

    test('should return a valid [NewsModel] when the json have a null values',
        () {
      // arrange

      final jsonMap =
          jsonDecode(fixture('news_with_null.json')) as Map<String, dynamic>;

      // act

      final result = NewsModel.fromJson(jsonMap);

      // assert

      expect(result, equals(tNewsModelWithNull));
    });
  });

  group('to json', () {
    test('should return a json map with the proper data', () {
      // arrange

      final expectedMap = {
        'source': {'name': '_empty.name'},
        'author': '_empty.author',
        'title': '_empty.title',
        'description': '_empty.description',
        'url': '_empty.url',
        'urlToImage': '_empty.urlToImage',
        'publishedAt': '_empty.publishedAt',
        'content': '_empty.content',
      };

      // act

      final result = tNewsModel.toJson();

      // assert

      expect(result, equals(expectedMap));
    });
  });
}
