import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:news_app/core/constants/constructors/news_models.dart';
import 'package:news_app/core/error/exceptions.dart';
import 'package:news_app/core/error/failures.dart';
import 'package:news_app/core/network/network_info.dart';
import 'package:news_app/features/daily_news/data/datasources/daily_news_local_data_source.dart';
import 'package:news_app/features/daily_news/data/datasources/daily_news_remote_data_source.dart';
import 'package:news_app/features/daily_news/data/repos/daily_news_repo_impl.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';

class MockDailyNewsRemoteDataSource extends Mock
    implements DailyNewsRemoteDataSource {}

class MockDailyNewsLocalDataSource extends Mock
    implements DailyNewsLocalDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  late DailyNewsRemoteDataSource remoteDataSource;
  late DailyNewsLocalDataSource localDataSource;
  late NetworkInfo networkInfo;
  late DailyNewsRepoImpl repo;

  const tNewsModels = [NewsModels.empty];
  const List<News> tNews = tNewsModels;

  const serverException = ServerException(
    message: 'Not found!',
    statusCode: '404',
  );
  final serverFailure = ServerFailure.fromException(serverException);

  const cacheException = CacheException(
    message: 'Not found!',
    statusCode: '404',
  );
  final cacheFailure = CacheFailure.fromException(cacheException);

  setUp(() {
    remoteDataSource = MockDailyNewsRemoteDataSource();
    localDataSource = MockDailyNewsLocalDataSource();
    networkInfo = MockNetworkInfo();
    repo = DailyNewsRepoImpl(
      remoteDataSource: remoteDataSource,
      localDataSource: localDataSource,
      networkInfo: networkInfo,
    );
  });

  test('should check if the internet connected', () async {
    // arrange

    when(() => networkInfo.isConnected).thenAnswer((_) async => true);
    when(() => remoteDataSource.getNews()).thenAnswer((_) async => tNewsModels);
    when(() => localDataSource.cacheNews(any()))
        .thenAnswer((_) => Future.value());

    // act

    await repo.getNews();

    // assert

    verify(() => networkInfo.isConnected);
  });

  group('device is online', () {
    setUp(() {
      when(() => networkInfo.isConnected)
          .thenAnswer((invocation) async => true);
    });

    test('should return a [List<News>] when completes successful', () async {
      // arrange

      when(() => remoteDataSource.getNews())
          .thenAnswer((_) async => tNewsModels);
      when(() => localDataSource.cacheNews(any()))
          .thenAnswer((invocation) => Future.value());

      // act

      final result = await repo.getNews();

      // assert

      expect(result, equals(const Right<dynamic, List<News>>(tNews)));
    });

    test('should cache the data when completes successful', () async {
      // arrange

      when(() => remoteDataSource.getNews())
          .thenAnswer((_) async => tNewsModels);
      when(() => localDataSource.cacheNews(any()))
          .thenAnswer((invocation) => Future.value());

      // act

      await repo.getNews();

      // assert
      verify(() => localDataSource.cacheNews(tNewsModels));
    });

    test('should return a [ServerFailure] when completes unsuccessful',
        () async {
      // arrange

      when(() => remoteDataSource.getNews()).thenThrow(serverException);
      when(() => localDataSource.cacheNews(any()))
          .thenAnswer((invocation) => Future.value());

      // act

      final result = await repo.getNews();

      // assert
      verifyZeroInteractions(localDataSource);
      expect(result, equals(Left<ServerFailure, List<News>>(serverFailure)));
    });
  });

  group('device is offline', () {
    setUp(() {
      when(() => networkInfo.isConnected)
          .thenAnswer((invocation) async => false);
    });

    test('should return proper [List<NewsModel>] when cached data is found',
        () async {
      // arrange

      when(() => localDataSource.getNews())
          .thenAnswer((_) async => tNewsModels);

      // act

      final result = await repo.getNews();

      // assert
      verifyZeroInteractions(remoteDataSource);
      verify(() => localDataSource.getNews());
      expect(result, equals(const Right<dynamic, List<News>>(tNews)));
    });

    test('should return [CacheFailure] when cached data is not found',
        () async {
      // arrange

      when(() => localDataSource.getNews()).thenThrow(cacheException);

      // act

      final result = await repo.getNews();

      // assert

      expect(result, equals(Left<Failure, dynamic>(cacheFailure)));
    });
  });
}
