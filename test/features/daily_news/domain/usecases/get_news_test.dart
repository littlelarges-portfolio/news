import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:news_app/core/constants/constructors/news.dart';
import 'package:news_app/features/daily_news/domain/entities/news.dart';
import 'package:news_app/features/daily_news/domain/repos/daily_news_repo.dart';
import 'package:news_app/features/daily_news/domain/usecases/get_news.dart';

class MockDailyNewsRepo extends Mock implements DailyNewsRepo {}

void main() {
  late GetNews usecase;
  late DailyNewsRepo repo;

  setUp(() {
    repo = MockDailyNewsRepo();
    usecase = GetNews(repo);
  });

  const tNews = [NewsEntities.empty];

  test('should call the [DailyNewsRepo.getNews] and return the same data',
      () async {
    // arrange
    when(() => repo.getNews()).thenAnswer((_) async => const Right(tNews));

    // act
    final result = await usecase();

    // assert
    expect(result, equals(const Right<dynamic, List<News>>(tNews)));
    verify(() => repo.getNews()).called(1);
    verifyNoMoreInteractions(repo);
  });
}
