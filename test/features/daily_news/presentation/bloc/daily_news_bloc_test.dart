import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:news_app/core/constants/constructors/news.dart';

import 'package:news_app/core/error/failures.dart';
import 'package:news_app/features/daily_news/domain/usecases/get_news.dart';
import 'package:news_app/features/daily_news/presentation/bloc/daily_news_bloc.dart';

class MockGetNews extends Mock implements GetNews {}

class MockFailure extends Mock implements Failure {}

void main() {
  late MockGetNews usecase;
  late DailyNewsBloc bloc;

  setUp(() {
    usecase = MockGetNews();
    bloc = DailyNewsBloc(getNews: usecase);
  });

  test('initial state is [Empty]', () {
    expect(bloc.state, equals(const Empty()));
  });

  group('on fetch', () {
    const tNews = NewsEntities.empty;
    final tNewsList = [tNews];

    final failure = MockFailure();

    test('should emit [Loading, Loaded] when [GetNews] returns a proper value',
        () {
      // arrange

      when(() => usecase()).thenAnswer((_) async => Right(tNewsList));

      // assert

      final expectedOrder = [
        const Loading(),
        Loaded(news: tNewsList),
      ];

      expectLater(bloc.stream, emitsInOrder(expectedOrder));

      // act

      bloc.add(const FetchDailyNews());
    });

    test('should emit [Loading, Error] when [GetNews] returns a [Failure]', () {
      // arrange
      when(() => failure.message).thenReturn('Something went wrong!');
      when(() => failure.statusCode).thenReturn('404');
      when(() => usecase()).thenAnswer((_) async => Left(failure));

      // assert

      final expectedOrder = [
        const Loading(),
        Error(message: failure.message),
      ];

      expectLater(bloc.stream, emitsInOrder(expectedOrder));

      // act

      bloc.add(const FetchDailyNews());
    });
  });
}
